#!/bin/bash
#SBATCH --job-name=generate-mcds
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 24
#SBATCH --mem=64G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

wd=/project/spott/qiaoshan/scNOMe-seq/test
lib=CEMBA200709_9E_4-1-E18
cpu=24

cd $wd

allcools generate-mcds \
    --allc_table mapping/stats/AllcPaths.tsv \
    --output_prefix mcds/$lib \
    --chrom_size_path /project/spott/reference/mouse/GRCm39/mm39.chrom.sizes \
    --mc_contexts CGN CHN \
    --bin_sizes 100000 \
    --cov_cutoff 2 \
    --cpu $cpu


