#!/bin/bash
#SBATCH --job-name=generate-mcds_geneslop2k
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 24
#SBATCH --mem=64G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

wd=/project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5
lib=SP-HE-scNOME-MW210318
cpu=24
chrom_sizes=/project/spott/reference/human/GRCh38/hg38.chrom.sizes
geneslop2k=/project/spott/reference/human/GRCh38/gencode.v43.geneslop2k.bed

cd $wd

allcools generate-mcds \
    --allc_table mapping/stats/AllcPaths.tsv \
    --output_prefix mcds/$lib \
    --chrom_size_path $chrom_sizes \
    --mc_contexts HCGN GCHN \
    --bin_sizes 100000 50000 25000 \
    --region_bed_paths $geneslop2k \
    --region_bed_names geneslop2k \
    --cov_cutoff 2 \
    --cpu $cpu


