#!/bin/bash
#SBATCH --job-name=snakemake
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=60G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -a 0-63
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

group=(../Group*)
g=`echo ${group[$SLURM_ARRAY_TASK_ID]} | perl -lane '$_=~s/^.*\///; print $_'`

dir=/project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mapping/$g
file=/project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mapping/$g/Snakefile

snakemake -d $dir --snakefile $file -j all --default-resources mem_mb=100 --resources mem_mb=50000


