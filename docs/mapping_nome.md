# Mapping Cell-level FASTQ Files

This protocol was tested on human NOMe-seq data at `/project/spott/spott/scNOMe-seq/heart_atlas_pilot5/`.

## I. prepare references

1\. create a directory to store the reference files
```
mkdir -p /project/spott/reference/human/GRCm38
```

2\. download the reference
```
cd /project/spott/reference/mouse/GRCm39
wget --timestamping 'ftp://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/*'
```

## II. prepare genome index

1\. build bismark index

Since bismark requires a clean directory containing genome fasta only, we need to create it at first:
```
mkdir genome
mv hg38.fa.gz genome/
gzip -d genome/hg38.fa.gz
```

```{Note}
Do NOT put any other fasta files in the `genome` directory as it will make bismark failed in mapping.
```

Then start building index:
```
cd /project/spott/reference/scripts
sbatch bismark_genome_preparation_hg38.sh
```

`bismark_genome_preparation_hg38.sh` content:
```
#!/bin/bash
#SBATCH --job-name=bismark_genome_preparation_hg38
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=48G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

bismark_genome_preparation /project/spott/reference/human/GRCh38/genome
```

2\. Build the standard HISAT-3N index (with C to T conversion) and the repeat HISAT-3N index
```
cd /project/spott/reference/scripts
sbatch hisat-3n-build_hg38.sh
```

`hisat-3n-build_hg38.sh` content:
```
#!/bin/bash
#SBATCH --job-name=hisat-3n-build_hg38
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=256G
#SBATCH --partition=bigmem
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

mkdir -p /project/spott/reference/human/GRCh38/hisat3n_index
/project/spott/software/hisat-3n/hisat-3n-build --base-change C,T --repeat-index /project/spott/reference/human/GRCh38/genome/hg38.fa /project/spott/reference/human/GRCh38/hisat3n_index/hg38
```

3\. Build the samtools index (required for bam-to-allc)
```
cd /project/spott/reference/scripts
sbatch faidx_hg38.sh
```

`faidx_hg38.sh` content:
```
#!/bin/bash
#SBATCH --job-name=faidx_hg38
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=48G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

samtools faidx /project/spott/reference/human/GRCh38/genome/hg38.fa
```

## III. prepare YAP mapping config file

1\. activate yap environment
```
conda activate yap
```

2\. create the project directory
```
mkdir /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5
```

3\. get the mapping config template
```
yap default-mapping-config --mode mc --barcode_version V1 --bismark_ref /project/spott/reference/human/GRCh38/genome --hisat3n_dna_ref /project/spott/reference/human/GRCh38/genome/hg38 --hisat3n_rna_ref HISAT3N_RNA_REF --genome_fasta /project/spott/reference/human/GRCh38/genome/hg38.fa --chrom_size_path /project/spott/reference/human/GRCh38/hg38.chrom.sizes --nome > mapping_config.ini
```

The `mc` mode doesn't use a hisat3n RNA reference. But the parameter is still needed for `yap default-mapping-config` to execute. A fake value for `--hisat3n_rna_ref` can be used as a placeholder here to make the command work.

4\. edit the parameters in [mapping_config.ini](https://gitlab.com/qiaoshan.lin/scnome-seq/-/blob/main/heart_atlas_pilot5/mapping_config.ini) when needed

```{Note}
I tried to use hisat3n_dna_ref for the mapping at first. But the run failed with the error message "NameError: The name 'bismark_reference' is unknown in this context. Please make sure that you defined that variable. Also note that braces not used for variable access have to be escaped by repeating them, i.e. {{print $1}}".

So I used bismark index at the end by commenting out the `hisat3n*reference` parameters as [here](https://gitlab.com/qiaoshan.lin/scnome-seq/-/blob/main/heart_atlas_pilot5/mapping_config.ini).
```

## IV. Mapping

1\. create a directory to store fastq files
```
mkdir /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/fastq
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/fastq
```

2\. softlink fastq files and rename them as YAP only recognize files ended with `-R1.fq.gz` and `-R2.fq.gz`.
```
for file in /project/spott/spott/scNOMe-seq/heart_atlas_pilot5/data/fastq/*; do ln -s "$file" .; done
for file in *; do mv -- "$file" "${file//_001/}"; done
rename _R1 \\-R1 *
rename _R2 \\-R2 *
rename \\- - *
rename fastq fq *
```

3\. prepare `Snakefile` 
```
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5
yap start-from-cell-fastq -o mapping -config mapping_config.ini -fq fastq/SP-HE-scNOME-MW210318-"*".fq.gz
```
3072 fq.gz files were randomly seperated into 64 groups. Each group has 48 fq.gz files.  

4\. prepare [snakemake.sh](https://gitlab.com/qiaoshan.lin/scnome-seq/-/tree/main/heart_atlas_pilot5/mapping/snakemake/snakemake.sh) for sbatch job array submission
```
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mapping/snakemake
sbatch snakemake.sh
```

The mapping took about 3 hours to finish.

Once the job is completed, check whether `MappingSummary.csv.gz` exists in each Group directory. If it does, mapping was successfully done in that Group directory.

```{Note}
Errors occur during parsing the plate info, this happens when the input FASTQ file name is not generated by yap. The `yap summary` also can not generate html report due to missing the plate info. In this case, you need to add the plateinfo by yourself in order to make the plate view plots. These information is not necessary for following analysis though.
```

5\. summarize the results

Although html report cannot be genertated at this point without plate info, we still need to run `yap summary` to generate an input for the next step.

```
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mapping
yap summary -o .
```

6\. run [generate-mcds.sh](https://gitlab.com/qiaoshan.lin/scnome-seq/-/tree/main/heart_atlas_pilot5/mcds/generate-mcds.sh) to generate mcds from allc files
```
mkdir /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mcds
cd /project/spott/qiaoshan/scNOMe-seq/heart_atlas_pilot5/mcds
sbatch generate-mcds.sh
```

Ready for single-cell level analysis.

