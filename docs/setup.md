# Pipeline Installation and Environmental Setup

## I. install miniconda

Refer to [condo user-guide](https://conda.io/projects/conda/en/stable/user-guide/install/linux.html).

1\. go to the software directory
```
cd /project/spott/software
```

2\. download the installer for python3.8:

```
wget https://repo.anaconda.com/miniconda/Miniconda3-py38_22.11.1-1-Linux-x86_64.sh
```

3\. verify installer hashes:
```
sha256sum Miniconda3-py38_22.11.1-1-Linux-x86_64.sh
```

4\. run the installer:
```
bash Miniconda3-py38_22.11.1-1-Linux-x86_64.sh
```

- specify `/project/spott/software/miniconda3` as the location for miniconda3 to install

- initialize Miniconda3: yes

5\. set conda's base environment not to be activated on startup: 

```
conda config --set auto_activate_base false
```

6\. restart shell

7\. show packages: 
```
conda list
```

## II. install ALLCools

1\. add channels:
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```

2\. install mamba in base environment: 
```
conda install mamba -n base -c conda-forge
```

3\. create /project/spott/software/yaml/allcools_env.yaml with the setup below: 
```
name: allcools
channels:
  - conda-forge
  - bioconda
  - defaults
dependencies:
  - python=3.8
  - pip
  - anndata
  - biopython
  - dask
  - numba
  - htslib>=1.9
  - jupyter
  - jupyter_contrib_nbextensions
  - leidenalg
  - natsort
  - netCDF4
  - networkx
  - opentsne
  - plotly
  - pybedtools
  - pyBigWig
  - pynndescent
  - pysam
  - pytables
  - scanpy
  - scikit-learn
  - seaborn
  - statsmodels
  - xarray
  - yaml
  - zarr
  - pip:
      - papermill
      - imblearn
      - allcools
```

4\. enter base env:
```
conda activate base
```

5\. create allcools environment and install all packages in the yaml files:
```
mamba env create -f /project/spott/software/yaml/allcools_env.yaml
```

6\. install optional packages:
```
mamba install -n allcools rpy2
mamba install -n allcools tpot xgboost dask-ml scikit-mdr skrebate
```

7\. re-install samtools:
```
mamba uninstall -c bioconda samtools
pip install --upgrade allcools
```

8\. re-install pybedtools
```
mamba install -n allcools pybedtools

```

9\. enter allcools env and test:
```
conda activate allcools
python /project/spott/software/test.py
```

`test.py` content:
```
import pandas as pd
import numpy as np
import scanpy as sc
import matplotlib.pyplot as plt

from ALLCools.mcds import MCDS
from ALLCools.clustering import tsne, significant_pc_test, log_scale
from ALLCools.plot import *
```

## III. install YAP

1\. create /project/spott/software/yaml/yap_env.yaml with the setup below: 
```
name: yap
channels:
  - conda-forge
  - bioconda
  - defaults
dependencies:
  - python=3.7
  - pip
  - jupyter
  - jupyter_contrib_nbextensions
  - cutadapt=2.10
  - snakemake=5.17
  - bismark=0.20
  - samtools=1.9
  - picard
  - bedtools=2.29
  - star=2.7.3a
  - subread=2.0
  - bowtie2=2.3
  - bowtie=1.3
  - htslib=1.9
  - pysam=0.15
  - pytables
  - seaborn
  - matplotlib
  - pip:
    - papermill
```

2\. create yap environment and install all packages in the yaml files:
```
mamba env create -f /project/spott/software/yaml/yap_env.yaml
```

3\. enter yap env:
```
conda activate yap
```

4\. install extra packages:
```
pip install cemba-data
pip install allcools
pip install zarr
pip install pandas
pip install Black
```

5\. install HISAT-3N (referring to the [instruction](http://daehwankimlab.github.io/hisat2/hisat-3n/))
```
cd /project/spott/software
git clone https://github.com/DaehwanKimLab/hisat2.git hisat-3n

cd hisat-3n
git checkout -b hisat-3n origin/hisat-3n

make
```

## IV. install and run jupyterlab

1\. run jupyter lab on midway3:

```
mamba install -n allcools jupyterlab
conda activate allcools
jupyter lab --no-browser --port=8886 --notebook-dir=/project/spott
```

Then, open a port on the Macbook firewall following steps 2-5.

2\. stop the pf (packet filter) firewall as it's active: 
```
sudo pfctl -d
```

3\. edit firewall's configuration (with sudo permission) by adding the following two lines to the end of `/etc/pf.conf`:
```
# my custom firewall rule
pass in inet proto tcp from any to any port 8889 no state
```

4\. reload the firewall's configuration from the edited `/etc/pf.conf`:
```
sudo pfctl -f /etc/pf.conf
```

5\. restart the firewall:
```
sudo pfctl -E
```

Now port 8899 has been opened on the firewall.

6\. forward the local port to the remote port:
```
ssh -N -f -L localhost:8889:localhost:8886 qiaoshan@midway3.rcc.uchicago.edu
```

open http://localhost:8889/ in a browser and enter the token copied from the server. 

Now you should see the jupyterlab interface on your browser.

## V. use jupyter-book to post documents on web

1\. install jupyter-book
```
mamba install -n base jupyter-book
mamba install -n allcools jupyter-book
```

2\. create a project folder
```
mkdir /project/spott/qiaoshan/scNOMe-seq
```

3\. create a book template
```
conda activate base
jupyter-book create /project/spott/qiaoshan/scNOMe-seq/docs/
```

4\. edit files in `docs`

5\. create and edit `/project/spott/qiaoshan/scNOMe-seq/.gitlab-ci.yml` 

See more at https://gitlab.com/pages/jupyterbook 

6\. create a blank GitLab repository named [scNOMe-seq](https://gitlab.com/qiaoshan.lin/scnome-seq)

7\. push documents to the remote repository 
```
cd /project/spott/qiaoshan/scNOMe-seq
git init

git add docs/
git commit -m 'add docs'

git remote add origin git@gitlab.com:qiaoshan.lin/scnome-seq.git
git push origin main
```

A few minutes later, you should see the notebook at https://qiaoshan.lin.gitlab.io/scnome-seq/







