# Mapping Cell-level FASTQ Files

This protocol uses the sample data from [YAP GitBook](https://hq-1.gitbook.io/mc/inhouse-demultiplex)

## I. prepare references

1\. create a directory to store the reference files
```
mkdir -p /project/spott/reference/mouse/GRCm39
```

2\. download the reference
```
cd /project/spott/reference/mouse/GRCm39
wget --timestamping 'ftp://hgdownload.soe.ucsc.edu/goldenPath/mm39/bigZips/*'
```

## II. prepare genome index

1\. build bismark index

Since bismark requires a clean directory containing genome fasta only, we need to create it at first:
```
mkdir genome
mv mm39.fa.gz genome/
gzip -d genome/mm39.fa.gz
```

```{Note}
Do NOT put any other fasta files in the `genome` directory as it will make bismark failed in mapping. 
```

Then start building index:
```
cd /project/spott/reference/scripts
sbatch bismark_genome_preparation_mm39.sh
```

`bismark_genome_preparation_mm39.sh` content:
```
#!/bin/bash
#SBATCH --job-name=bismark_genome_preparation_mm39
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=48G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

bismark_genome_preparation /project/spott/reference/mouse/GRCm39/genome
```

2\. Build the standard HISAT-3N index (with C to T conversion) and the repeat HISAT-3N index
```
cd /project/spott/reference/scripts
sbatch hisat-3n-build_mm39.sh
```

`hisat-3n-build_mm39.sh` content:
```
#!/bin/bash
#SBATCH --job-name=hisat-3n-build_mm39
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=256G
#SBATCH --partition=bigmem
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

mkdir -p /project/spott/reference/mouse/GRCm39/hisat3n_index
/project/spott/software/hisat-3n/hisat-3n-build --base-change C,T --repeat-index /project/spott/reference/mouse/GRCm39/genome/mm39.fa /project/spott/reference/mouse/GRCm39/hisat3n_index/mm39
```

3\. Build the samtools index (required for bam-to-allc)
```
cd /project/spott/reference/scripts
sbatch faidx_mm39.sh
```

`faidx_mm39.sh` content:
```
#!/bin/bash
#SBATCH --job-name=faidx_mm39
#SBATCH --account=pi-spott
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=48G
#SBATCH --partition=caslake
#SBATCH --mail-type=END
#SBATCH --mail-user=qiaoshan@uchicago.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source /project/spott/software/miniconda3/etc/profile.d/conda.sh
conda activate yap

samtools faidx /project/spott/reference/mouse/GRCm39/genome/mm39.fa
```

## III. prepare YAP mapping config file

1\. activate yap environment
```
conda activate yap
```

2\. create the project directory
```
mkdir /project/spott/qiaoshan/scNOMe-seq/test
cd /project/spott/qiaoshan/scNOMe-seq/test
```

3\. get the mapping config template
```
yap default-mapping-config --mode mc --barcode_version V2 --bismark_ref /project/spott/reference/mouse/GRCm39/genome --genome_fasta /project/spott/reference/mouse/GRCm39/genome/mm39.fa --chrom_size_path /project/spott/reference/mouse/GRCm39/mm39.chrom.sizes > mapping_config.ini
```

4\. edit the parameters in [mapping_config.ini](https://gitlab.com/qiaoshan.lin/scnome-seq/-/blob/main/test/mapping_config.ini) when needed
```{Note}
By default, YAP uses the HISAT-3N index when hisat3n_dna_ref is provided. If you want to use bismark index instead, you need to delete or comment out the `hisat3n*reference` parameters as [here](https://gitlab.com/qiaoshan.lin/scnome-seq/-/blob/main/test/mapping_config.ini), otherwise the definition of `bismark_reference` will be ignored in the following mapping step.

```

## IV. Mapping

1\. create a directory to store fastq files
```
mkdir /project/spott/qiaoshan/scNOMe-seq/test/fastq
cd /project/spott/qiaoshan/scNOMe-seq/test/fastq
```

2\. download sample fastq files from [YAP GitBook](https://hq-1.gitbook.io/mc/inhouse-demultiplex)

3\. prepare `Snakefile` 
```
cd /project/spott/qiaoshan/scNOMe-seq/test
yap start-from-cell-fastq -o mapping -config mapping_config.ini -fq fastq/CEMBA200709_9E_4-1-E18-"*".fq.gz
```

<span style="color:orange">*Attention: the max group number is 64 and the YAP sample data has exactly 64 pairs, don't known what could happen when fastq pairs > 64*</span>

4\. prepare [snakemake.sh](https://gitlab.com/qiaoshan.lin/scnome-seq/-/tree/main/test/mapping/snakemake/snakemake.sh) for sbatch job array submission
```
cd /project/spott/qiaoshan/scNOMe-seq/test/mapping/snakemake
sbatch snakemake.sh
```

Once the job is completed, check whether `MappingSummary.csv.gz` exists in each Group directory. If it does, mapping was successfully done in that Group directory.

<span style="color:orange">*For this testing of YAP sample data, Group18 fastq file was somehow broken. So I removed this Group from the downstream analysis.*</span>

5\. summarize the results
```
cd /project/spott/qiaoshan/scNOMe-seq/test/mapping
yap summary -o .
cp ./stats/MappingSummary.ipynb ../../docs/
```
Check summary at [MappingSummary.html](https://qiaoshan.lin.gitlab.io/scnome-seq/MappingSummary.html)

<span style="color:orange">*You may need to change the cutoff values based on how the library was loaded.*</span>

To customize your own notebook template for your specific experimental setup, edit the default [MappingSummary.ipynb](https://gitlab.com/qiaoshan.lin/scnome-seq/-/tree/main/docs/MappingSummary.ipynb) file and save it to `MappingSummary_customized.ipynb`; then include it in the command:
```
yap summary -o . -nb MappingSummary_customized.ipynb
```

6\. run [generate-mcds.sh](https://gitlab.com/qiaoshan.lin/scnome-seq/-/tree/main/test/mcds/generate-mcds.sh) to generate mcds from allc files
```
mkdir /project/spott/qiaoshan/scNOMe-seq/test/mcds
cd /project/spott/qiaoshan/scNOMe-seq/test/mcds
sbatch generate-mcds.sh
```

Ready for single-cell level analysis.

