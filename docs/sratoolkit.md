# sratoolkit usage

### I. install sratoolkit

1\. download installer
```
wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/3.0.2/sratoolkit.3.0.2-centos_linux64.tar.gz
```

2\. decompress 
```
tar -vxzf sratoolkit.3.0.2-centos_linux64.tar.gz
```

3\. add to PATH
```
export PATH=$PATH:/project/spott/software/sratoolkit.3.0.2-centos_linux64/bin
```

4\. create a directory as data depository
```
mkdir /project/spott/ncbi
```

5\. configuration
```
vdb-config -i
```
- enable remote access
- enable local file-caching
- configure the cache file location to /project/spott/ncbi where prefetch will deposit the files
- enable report cloud instance identity

6\. test 
```
fastq-dump --stdout -X 2 SRR390728
```

### II. download data

1\. create a directory for storing the data
```
mkdir /path/to/data/repo
```

2\. create `SRR_Acc_List.txt` containing a list of SRR accession numbers, for example:
```
SRR8994458
SRR8994459
SRR8994460
SRR8994461
SRR8994462
SRR8994463
SRR8994464
SRR8994465
SRR8994466
```

3\. create a bash script `sra_dump.sh`
```
readarray -t a < SRR_Acc_List.txt

for i in {0..9}
do
        prefetch ${a[$i]}
        fasterq-dump ${a[$i]}
done
```

4\. run the script
```
sh sra_dump.sh
```


